module SessionsHelper

  def current_user
    @current_user ||= User.find_by_token(sessions[:token])
  end

  def current_user=(user)
    @current_user = user
    sessions[:token] = user.token
  end

  def login_user(user)
    current_user = user
  end

  def logout
    current_user.reset_token!
    @current_user = nil
    sessions[:token] = nil
  end

end
