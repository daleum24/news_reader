class SessionsController < ApplicationController

  def create
    begin
      puts sessions[:token]
      @user = User.find_by_credentials!(params[:user])
      login_user(@user)
      render json: @user
    rescue StandardError => e
      render json: {error: e.message}.to_json, status: 422
    end
  end

  def destroy
    log_out
    render json: {message: 'logged out'}
  end

end
