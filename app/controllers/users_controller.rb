class UsersController < ApplicationController

  def create
    begin
      @user = User.create!(params[:user])
      login_user(@user)
      render json: @user
    rescue StandardError => e
      render json: {error: e.message}.to_json, status: 422}
    end
  end

end
