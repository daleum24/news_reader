require "bcrypt"

class User < ActiveRecord::Base
  attr_accessible  :username, :password
  attr_reader :password

  validates :username, presence: true, uniqueness: true
  validates :password, presence: true, length: { minimum: 6 }
  validates :password_digest, presence: true
  validates :token, presence: true

  after_initialize :reset_token!

  def reset_token!
    self.update_attribute("token", SecureRandom.urlsafe_base64)
    self.save!
  end

  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end

  def has_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end

  def self.find_by_credentials!(user)
    @user = User.find_by_username!(user.username)
    raise Error unless @user.has_password?(user.password)
    @user
  end

end
