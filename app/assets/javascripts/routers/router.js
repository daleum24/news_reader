NR.Routers.Router = Backbone.Router.extend({
  initialize: function(feeds){
    this.rootEl = $("#content");
    this.feeds = feeds;
    this.installUserView();
  },

  routes: {
    "": "index",
    "feeds/:id": "show",
    "feeds/:feed_id/entries/:id": "showEntry"
  },

  index: function(){
    var indexView = new NR.Views.IndexView(this.feeds);
    this.rootEl.html(indexView.render().$el);
  },

  show: function(id){

    var feed = this.feeds.get(id);
    var feedView = new NR.Views.FeedView(feed);

    this._swapView(feedView);
    // this.rootEl.html(feedView.render().$el);

  },

  installUserView: function(){
    var that = this;


    NR.user.save(null,{
      success: function(){
        var userView = new NR.Views.UserView();
        userView.render();
      },
      error: function(){
        var signInView = new NR.Views.signInView();
        signInView.render();
      }
    });

  },

  showEntry: function(feed_id, id){
    var feed = this.feeds.get(feed_id);
    var entry = feed.get("entries").get(id);
    var entryView = new NR.Views.EntryView(entry);

    this.rootEl.html(entryView.render().$el);
  },

  _swapView: function (newView) {
    this._currentView && this._currentView.remove();
    this._currentView = newView;
    this.rootEl.html(newView.render().$el);
  }
});