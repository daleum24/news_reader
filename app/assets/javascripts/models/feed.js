NR.Models.Feed = Backbone.Model.extend({
  initialize: function(){

    var entries = new NR.Collections.Entries(this.get('entriesJSON'),{parse:true});

    this.set('entries', entries );

  },

  parse: function(response){
    console.log(response);
  }
});