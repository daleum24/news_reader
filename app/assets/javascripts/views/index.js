NR.Views.IndexView = Backbone.View.extend({
  initialize: function(feeds){
    this.collection = feeds;
  },

  template: JST['index'],

  render: function(){
    this.$el.html(this.template({
      feeds: this.collection.models
    }));

    return this;
  }

});