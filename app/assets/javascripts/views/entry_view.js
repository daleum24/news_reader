NR.Views.EntryView = Backbone.View.extend({
  initialize: function(entry){
    this.model = entry;
  },

  template: JST['show_entry'],

  render: function(){
    console.log(this.model.get("json"));
    this.$el.html(this.template({
      entry: this.model
    }));

    return this;
  }

});