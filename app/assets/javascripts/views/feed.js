NR.Views.FeedView = Backbone.View.extend({
  initialize: function(feed){
    var that = this;
    this.model = feed;
    this.model.get('entries').url = '/feeds/' + feed.get('id') + '/entries';
    this.timer = setInterval(function() {
        console.log('sent');
        that.model.get('entries').fetch();
    }, 5000);

    var events = ["add", "change:title", "remove", "reset"];
        _(events).each(function (event) {
          that.listenTo(that.model.get('entries'), event, that.render);
        });
  },

  close: function() {

  },

  remove: function() {
    this.$el.remove();
    clearInterval(this.timer);
    this.stopListening();
    return this;
  },


  events: {
    'click .refresh': 'refresh'
  },

  refresh: function(){
    that = this;
    this.entries = this.model.get('entries').fetch({success: function(){
      that.render();
    }});

    // Backbone.history.navigate("feeds/"+this.model.get("id"), {trigger: true})
  },

  template: JST['feed'],

  render: function(){

    this.$el.html(this.template({
      feed: this.model,
      entries: this.model.get('entries').models
    }));

    return this;
  }
});