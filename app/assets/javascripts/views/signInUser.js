NR.Views.signInView = Backbone.View.extend({

  template: JST['sign_in'],

  events:{
    // "submit #user #login_form": "signUp",
    "click *": "signUp"
    // "click #user .login": "login"
  },

  signUp: function(event){
    event.preventDefault();
    NR.user.rootUrl = "/users"
    this.login(event);
  },

  login: function(event){
    event.preventDefault();
    var formData = $(event.currentTarget).find("form").serializeJSON();
    NR.user.save(formData, {
      success: function(){
        alert("User was saved!");
      },
      error: function(){
        alert("WRONG");
      }
    })
  },

  render: function(){
    $("#user").html(this.template());

    return this;
  }

});