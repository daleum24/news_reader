window.NR = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {

    var feedsObjs = JSON.parse($('#bootstrapped_feeds').html());
    var feedsCollection = new NR.Collections.Feeds(feedsObjs);

    var token = localStorage['token'];
    var userToken = {
      token: token
    };
    NR.user = new NR.Models.User(userToken);

    var routers = new NR.Routers.Router( feedsCollection );

    Backbone.history.start();
  }
};


